var info = document.getElementById('info');
var buttonAddRemove = document.getElementById('button5');

function back() {
    window.history.back();
}
controlWatchList();
info.innerHTML = "";
var movies = JSON.parse(localStorage.films);
var searchedMovieNumber = 0;
for (var i = 0; i <movies.length; i++){
    if (movies[i].id == localStorage.idN){
        searchedMovieNumber = i;
    }
}
info.innerHTML += "<h1>" + getName(searchedMovieNumber) + " (" +
    getDate(searchedMovieNumber) + ")</h1><br> <img src='" + getPhoto(searchedMovieNumber) +
    "' width='150' onerror='this.src=\"noimage.jpg\"'> <p>"+
    getOverview(searchedMovieNumber) + "</p> </ul>";


var url = "https://api.themoviedb.org/3/movie/"+ movies[searchedMovieNumber].id + "/credits?api_key=0bada6d9e9379a6891dbaf7dd42f275e";
$.get(url, function (getData) {
    console.log(getData);
    info.innerHTML += "<h3>Cast</h3><i id='cast'> ";
    for (i =0; i < getData.cast.length && i<10; i++ ){

        cast.innerHTML += JSON.stringify(getData.cast[i].name).replace(/"/g, "") + " - " +
            JSON.stringify(getData.cast[i].character).replace(/"/g, "") + "</br>"

    }


});
info.innerHTML += "</i>";

var url2 = "https://api.themoviedb.org/3/movie/"+ movies[searchedMovieNumber].id + "/videos?api_key=0bada6d9e9379a6891dbaf7dd42f275e&language=en-US";
$.get(url2, function (getData) {
    console.log(getData);
    info.innerHTML += "</br><iframe id='trailer' width='250' height='150' src='https://www.youtube.com/embed/" + JSON.stringify(getData.results[0].key).replace(/"/g, "") + "'> </iframe>";

});


function getName(searchedMovieNumber){
    name = movies[searchedMovieNumber].name;
    return name;
}

function getDate(searchedMovieNumber){
    date = movies[searchedMovieNumber].date;
    return date;
}

function getPhoto(searchedMovieNumber){
    photo = movies[searchedMovieNumber].photo;
    return photo;
}

function getOverview(searchedMovieNumber){
    overview = movies[searchedMovieNumber].overview;
    return overview;
}
function getVote_Average(searchedMovieNumber){
    vote_average = movies[searchedMovieNumber].vote_average;

    return vote_average;
}

function add() {
    if(buttonAddRemove.innerHTML == "Add"){
        watchlist = [];
        movie = {
            name: getName(searchedMovieNumber),
            date: getDate(searchedMovieNumber),
            photo: getPhoto(searchedMovieNumber),
            overview: getOverview(searchedMovieNumber),
            id: localStorage.idN,
            vote_average: getVote_Average(searchedMovieNumber)
        };

        if (localStorage.watchlist == null || localStorage.watchlist == ""){
            watchlist.push(movie);
            localStorage.watchlist = JSON.stringify(watchlist);
        } else {
            alreadyInWatchlist = JSON.parse(localStorage.watchlist);
            for (i =0; i < alreadyInWatchlist.length; i++ ){
                watchlist.push(alreadyInWatchlist[i]);

            }
            watchlist.push(movie);
            localStorage.watchlist = "";
            localStorage.watchlist = JSON.stringify(watchlist);

        }
        buttonAddRemove.innerHTML = "Remove";
    } else {
        remove();

    }


}
function openWatchList() {
    window.open('WatchList.html',"_self");
}
function openSearch() {
    window.open('index.html',"_self");
}
function controlWatchList(){
    if(localStorage.watchlist != "" && localStorage.watchlist != null){
        moviesWatchList = JSON.parse(localStorage.watchlist);
        for(var i = 0; i <moviesWatchList.length; i++){

            if(localStorage.idN == moviesWatchList[i].id){
                buttonAddRemove.innerHTML = "Remove";
                break;
            }else {
                buttonAddRemove.innerHTML = "Add";
            }
        }
    }

}
function remove() {
    moviesWatchList = JSON.parse(localStorage.watchlist);
    for(var i = 0; i <moviesWatchList.length; i++) {
        if (localStorage.idN == moviesWatchList[i].id) {
            moviesWatchList.splice(i , 1);
        }
    }
    localStorage.watchlist = "";
    localStorage.watchlist = JSON.stringify(moviesWatchList);
    buttonAddRemove.innerHTML = "Add";
}

