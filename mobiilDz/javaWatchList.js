
if(localStorage.watchlist!= null && localStorage.watchlist!=""){
    var movies = JSON.parse(localStorage.watchlist);
    var movieList = [];
    sort();
}


function sortByTime(){
    info.innerHTML = "";
    movies = JSON.parse(localStorage.watchlist);
    movieList = [];
    for (i = movies.length - 1; i > -1; i-- ){
        console.log(movies[i].name);
        info.innerHTML += "<li onclick='openDetailedWindow(" + movies[i].id + ")'><b>" + movies[i].name +
            " (" + movies[i].date + ")</b><br> <img src='" +
            movies[i].photo + "'  width='100' align='left' onerror='this.src=\"noimage.jpg\"' ><small><i>" +
            movies[i].overview + "</i> </small> </li> <br> ";
        movieList.push(createObject(i));

    }
    localStorage.films = "";
    localStorage.films = JSON.stringify(movieList);
}


function openDetailedWindow(id) {

    localStorage.idN = "";
    localStorage.idN = id;
    window.open('DetailedView.html',"_self");

}

function createObject(i) {
    movie = {
        name: movies[i].name,
        date: movies[i].date,
        photo: movies[i].photo,
        overview: movies[i].overview,
        id: movies[i].id,
        vote_average: movies[i].vote_average
    };

    return (movie);

}
function openWatchList() {
    window.open('WatchList.html',"_self");
}
function openSearch() {
    window.open('index.html',"_self");
}
function sort() {
    if(document.getElementById('sort').value == "time"){
        sortByTime();

    }
    if(document.getElementById('sort').value == "rating"){
        sortByRating();
    }
    if(document.getElementById('sort').value == "name"){
        sortByName();
    }
}


function sortByRating(){
    moviesByRating = movies;
    moviesByRating.sort(function(a, b) {
        return a.vote_average.localeCompare(b.vote_average);
    });

    info.innerHTML = "";
    for (i = moviesByRating.length - 1; i > -1; i-- ){

        info.innerHTML += "<li onclick='openDetailedWindow(" + moviesByRating[i].id + ")'><b>" + moviesByRating[i].name +
            " (" + moviesByRating[i].date + ")</b><br> <img src='" +
            moviesByRating[i].photo + "'  width='100' align='left' onerror='this.src=\"noimage.jpg\"' ><small><i>" +
            moviesByRating[i].overview + "</i> </small> </li> <br> ";


    }

}
function sortByName(){
    moviesByName = movies;
    moviesByName.sort(function(a, b) {
        return b.name.localeCompare(a.name);
    });

    info.innerHTML = "";
    for (i = moviesByName.length - 1; i > -1; i-- ){

        info.innerHTML += "<li onclick='openDetailedWindow(" + moviesByName[i].id + ")'><b>" + moviesByName[i].name +
            " (" + moviesByName[i].date + ")</b><br> <img src='" +
            moviesByName[i].photo + "'  width='100' align='left' onerror='this.src=\"noimage.jpg\"' ><small><i>" +
            moviesByName[i].overview + "</i> </small> </li> <br> ";


    }

}
