var info = document.getElementById('info');
function topList() {
    url = "https://api.themoviedb.org/3/movie/top_rated?api_key=0bada6d9e9379a6891dbaf7dd42f275e&language=en-US&page=1";
    info.innerHTML = "";
    $.get(url, function (getData) {
        console.log(getData);
        var movies = [];
        for (i =0; i < getData.results.length; i++ ){
            info.innerHTML += "<li onclick='openDetailedWindow(" + getId(i, getData) + ")'><b>" + getName(i, getData) +
                " (" + getDate(i, getData) + ")</b><br> <img src='" +
                getPhoto(i, getData) + "'  width='100' align='left' onerror='this.src=\"noimage.jpg\"' ><small><i>" +
                getOverview(i, getData) + "</i> </small> </li> <br> ";
            movies.push(createObject(i, getData));

        }
        localStorage.films = "";
        localStorage.films = JSON.stringify(movies);


    });
}

if (document.getElementById('input').value==""){
    topList();
}
else if (document.getElementById('input').value!=""){
    searchMovie();
}


function searchMovie() {
    info.innerHTML = "";
    if (document.getElementById('input').value == ""){
        topList();
    } else{

        var url = 'https://api.themoviedb.org/3/search/movie?api_key=0bada6d9e9379a6891dbaf7dd42f275e&language=en-US&query='
            + (document.getElementById('input').value) +'&page=1&include_adult=false';
        $.get(url, function (getData) {
            console.log(getData);
            var movies = [];
            for (i =0; i < getData.results.length; i++ ){
                info.innerHTML += "<li onclick='openDetailedWindow(" + getId(i, getData) + ")'><b>" + getName(i, getData) +
                    " (" + getDate(i, getData) + ")</b><br> <img src='" +
                    getPhoto(i, getData) + "'  width='100' align='left' onerror='this.src=\"noimage.jpg\"'><small><i>" +
                    getOverview(i, getData) + "</i> </small> </li> <br> ";
                movies.push(createObject(i, getData));

            }
            localStorage.films = "";
            localStorage.films = JSON.stringify(movies);

        });
    }

}
function getName(i, getData){
    name = JSON.stringify(getData.results[i].title).replace(/"/g, "");
    return name;
}

function getId(i, getData){
    id = JSON.stringify(getData.results[i].id).replace(/"/g, "")
    return id;
}

function getDate(i, getData){
    date = JSON.stringify(getData.results[i].release_date).replace(/"/g, "");
    if (date == ''){
        date = "Not known";
    }
    return date;
}

function getPhoto(i, getData){
    photo = "https://image.tmdb.org/t/p/w300" +
        JSON.stringify(getData.results[i].poster_path).replace(/"/g, "");
    if (photo == "https://image.tmdb.org/t/p/w300null"){
        photo = "noimage.jpg";
    }
    return photo;
}

function getOverview(i, getData){
    overview = JSON.stringify(getData.results[i].overview).replace(/"/g, "");

    if (overview == ''){
        overview = "Will be added soon";
    }
    return overview.replace(/\\/g, '"');
}

function getVote_Average(i, getData){
    vote_average = JSON.stringify(getData.results[i].vote_average).replace(/"/g, "");

    return vote_average;
}

function openDetailedWindow(id) {

    localStorage.idN = "";
    localStorage.idN = id;
    window.open('DetailedView.html',"_self");

}
function createObject(i, getData) {
    movie = {
        name: getName(i, getData),
        date: getDate(i, getData),
        photo: getPhoto(i, getData),
        overview: getOverview(i, getData),
        id: getId(i, getData),
        vote_average: getVote_Average(i, getData)
    };

    return (movie);

}
function openWatchList() {
    window.open('WatchList.html',"_self");
}
function openSearch() {
    window.open('index.html',"_self");
}
