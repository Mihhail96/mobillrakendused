package com.example.mihha.messenger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by mihha on 3/20/2017.
 */

public class Chat extends Topic {
    private FirebaseListAdapter<ChatMessage> adapter;

    public String userName = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
    public String userEmail = FirebaseAuth.getInstance().getCurrentUser().getEmail();
    public static String topic = null;
    private int PICK_IMAGE_REQUEST = 3;
    public static String imgUrl = "empty";
    public static int sort = 0;
    public String post = "";
    public static long lastMessageTime;
    public static Runnable runnable;
    public static Handler h2;
    private String page = "Chat";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        menu();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );




        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            topic=extras.getString("topic");
        }
        h2 = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                checkForDelete();
                deleteTopic();
                new DisplayChatMessages().execute();

                h2.postDelayed(this, 10000);
            }
        };
        runnable.run();

        checkForDelete();
        new DisplayChatMessages().execute();
        if (Topic.users!=null && Topic.users.size()>0){
            if (!Topic.users.contains(userEmail)){
                FirebaseDatabase.getInstance()
                        .getReference("topics/"+topic+"/users")
                        .push()
                        .setValue(new User(userEmail)
                        );
            }
        } else {
            FirebaseDatabase.getInstance()
                    .getReference("topics/"+topic+"/users")
                    .push()
                    .setValue(new User(userEmail)
                    );
        }
        FirebaseDatabase.getInstance().getReference().child("topics").child(topic).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    post = dataSnapshot.getValue(TopicObject.class).getType().toString();
                    sort = 0;
                    if (post.equals("fixed")){
                        sort = 1000;
                    }
                    updateData();
                }


            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
    void checkForDelete(){

        FirebaseDatabase.getInstance().getReference("topics/"+topic+"/messages").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot issue: dataSnapshot.getChildren()) {
                            long messageTime = Long.parseLong(issue.child("messageTime").getValue().toString());
                            long timeNow = new Date().getTime();
                            Date d1 = new Date(timeNow);
                            Date d2 = new Date(messageTime);
                            long diff = d1.getTime() - d2.getTime();


                            long diffMinutes = diff / (60 * 1000) % 60;
                            long diffHours = diff / (60 * 60 * 1000) % 24;
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            if (diffMinutes> 0 || diffHours>0 || diffDays>0) {
                                issue.getRef().setValue(null);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }
    void triggerAddMessage(View view){
        addNewMessage();
    }
    void deleteTopic(){
        FirebaseDatabase.getInstance().getReference().child("topics").child(topic).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    post = dataSnapshot.getValue(TopicObject.class).getType().toString();
                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FirebaseDatabase.getInstance().getReference("topics/"+topic).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        lastMessageTime = dataSnapshot.getValue(TopicObject.class).getLastMessageTime();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
        FirebaseDatabase.getInstance().getReference("topics/"+topic+"/messages").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final long numChildren = dataSnapshot.getChildrenCount();
                        if (post.equals("normal") && numChildren==0&& lastMessageTime!=0){
                            Toast.makeText(Chat.this, "Deleting Topic",
                                Toast.LENGTH_SHORT).show();
                            FirebaseDatabase.getInstance().getReference("topics/"+topic).setValue(null);
                            h2.removeCallbacks(runnable);
                            Intent intent  = new Intent(Chat.this, Topic.class);
                            startActivity(intent);
                            finish();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }
    void updateData(){
        FirebaseDatabase.getInstance().getReference("topics/"+topic+"/users").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final long numChildren = dataSnapshot.getChildrenCount();

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference ref = database.getReference("topics");
                        DatabaseReference hopperRef = ref.child(Chat.topic);
                        Map<String, Object> hopperUpdates = new HashMap<String, Object>();
                        hopperUpdates.put("numberOfUsers", numChildren*(-1)-sort);
                        hopperRef.updateChildren(hopperUpdates);


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }

    void addNewMessage(){
        checkForDelete();
        EditText input = (EditText)findViewById(R.id.input);

        // Read the input field and push a new instance
        // of ChatMessage to the Firebase database

        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            topic=extras.getString("topic");
        }
        if (userName==null){
            userName=userEmail;

        }
        String in = input.getText().toString();
        for (String word : Dispatcher.curseWords) {
            word = word.replaceAll("[^A-Za-z0-9]", "");
            Pattern rx = Pattern.compile("\\b" + word + "\\b", Pattern.CASE_INSENSITIVE);
            in = rx.matcher(in).replaceAll(new String(new char[word.length()]).replace('\0', '*'));
        }


        FirebaseDatabase.getInstance()
                .getReference("topics/"+topic+"/messages")
                .push()
                .setValue(new ChatMessage(in, userName, imgUrl)
                );

        addLastMessageTime(in);
        input.setText("");
        imgUrl = "empty";
    }
    void addLastMessageTime(final String inputMessage){

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("topics").child(topic).child("messages");
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                if(issue.child("messageText").getValue().toString().equals(inputMessage)){
                                    final long messageDate = Long.parseLong(issue.child("messageTime").getValue().toString());
                                    FirebaseDatabase.getInstance().getReference("topics/"+topic+"/messages").
                                            addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                                                    DatabaseReference ref = database.getReference("topics");
                                                    DatabaseReference hopperRef = ref.child(Chat.topic);
                                                    Map<String, Object> hopperUpdates = new HashMap<String, Object>();
                                                    hopperUpdates.put("lastMessageTime", messageDate);
                                                    hopperRef.updateChildren(hopperUpdates);

                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {}
                                            });
                                }
                            }


                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });
    }


    private class DisplayChatMessages extends AsyncTask<String,Void,Bitmap> {
        ListView listOfMessages;


        @Override
        protected Bitmap doInBackground(String... params) {

            listOfMessages = (ListView)findViewById(R.id.list_of_messages);
            Bundle extras = getIntent().getExtras();
            if (extras!=null){
                topic=extras.getString("topic");
            }


            adapter = new FirebaseListAdapter<ChatMessage>(Chat.this, ChatMessage.class,
                    R.layout.message, FirebaseDatabase.getInstance().getReference("topics/" + topic + "/messages")) {
                @Override
                protected void populateView(View v, ChatMessage model, int position) {

                    // Get references to the views of message.xml
                    TextView messageText = (TextView)v.findViewById(R.id.message_text);
                    TextView messageUser = (TextView)v.findViewById(R.id.message_user);
                    TextView messageText2 = (TextView)v.findViewById(R.id.message_text2);
                    TextView messageUser2 = (TextView)v.findViewById(R.id.message_user2);
                    ImageView pictureImage = (ImageView)v.findViewById(R.id.imageMessage1);

                    long messageTime = model.getMessageTime();
                    long timeNow = new Date().getTime();
                    Date d1 = new Date(timeNow);
                    Date d2 = new Date(messageTime);
                    long diff = d1.getTime() - d2.getTime();
                    long diffSeconds = diff / 1000 % 60;
                    float opacity = 1;
                    if (diffSeconds>10 && diffSeconds < 20){
                        opacity=0.7F;
                    } else if (diffSeconds>=20 && diffSeconds < 40){
                        opacity=0.5F;
                    } else if (diffSeconds>=40){
                        opacity=0.25F;
                    }

                    String user2 = model.getMessageUser();
                    String pictureUrl = model.getImageMessage();
                    if (userName==null){
                        userName=userEmail;

                    }
                    if(!user2.equals(userName)){
                        if (!pictureUrl.equals("empty")){
                            new DownLoadImage(pictureImage).execute(pictureUrl);
                            messageUser2.setText(model.getMessageUser());
                            messageUser2.setAlpha(opacity);
                        } else {
                            messageUser2.setText(model.getMessageUser());
                            messageUser2.setAlpha(opacity);
                            messageText2.setText(model.getMessageText());
                            messageText2.setAlpha(opacity);
                        }


                    } else{
                        if (!pictureUrl.equals("empty")){
                            new DownLoadImage(pictureImage).execute(pictureUrl);
                            messageUser.setText(model.getMessageUser());
                            messageUser.setAlpha(opacity);
                        }else {
                            messageUser.setText(model.getMessageUser());
                            messageUser.setAlpha(opacity);
                            messageText.setText(model.getMessageText());
                            messageText.setAlpha(opacity);
                        }


                    }

                }
            };



            return null;
        }
        private class DownLoadImage extends AsyncTask<String,Void,Bitmap> {
            ImageView imageView;

            public DownLoadImage(ImageView imageView){
                this.imageView = imageView;
            }

            protected Bitmap doInBackground(String...urls){
                String urlOfImage = urls[0];
                Bitmap logo = null;
                try{
                    InputStream is = new URL(urlOfImage).openStream();
                    logo = BitmapFactory.decodeStream(is);
                }catch(Exception e){ // Catch the download exception
                    e.printStackTrace();
                }
                return logo;
            }

            protected void onPostExecute(Bitmap result){
                imageView.setImageDrawable(new BitmapDrawable(getResources(),result));
            }
        }
        protected void onPostExecute(Bitmap res){
            if (listOfMessages != null){
                listOfMessages.setAdapter(adapter);
            }

        }


    }

    void findPicture(View view){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE_REQUEST);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));
                Drawable d = new BitmapDrawable(getResources(), bitmap);
                ImageView imageView = (ImageView) findViewById(R.id.imagepic);
                imageView.setImageDrawable(d);
                serverPicture(imageView);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    void serverPicture(ImageView imageView){
        imageView.setDrawingCacheEnabled(true);

        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        StorageReference storageRef;
        storageRef = FirebaseStorage.getInstance().getReference();
        Random rn = new Random();
        int answer = rn.nextInt(100000000) + 1;
        StorageReference mountainsRef = storageRef.child(answer+".jpg");
        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests")
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                imgUrl = downloadUrl.toString();
                addNewMessage();

            }
        });
    }
    void menu(){
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.profile) {
                    h2.removeCallbacks(runnable);
                    Intent intent  = new Intent(Chat.this, Profile.class);
                    startActivity(intent);
                    finish();
                    return true;
                }if (item.getItemId() == R.id.topic) {
                    h2.removeCallbacks(runnable);
                    Intent intent  = new Intent(Chat.this, Topic.class);
                    startActivity(intent);
                    finish();
                    return true;
                }if (item.getItemId() == R.id.createTopic) {
                    h2.removeCallbacks(runnable);
                    Intent intent  = new Intent(Chat.this, CreateTopic.class);
                    startActivity(intent);
                    finish();
                    return true;
                }
                return false;
            }
        });
    }
    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", Topic.class.getName());
        editor.commit();
    }

}
