package com.example.mihha.messenger;

import android.app.AlertDialog;

import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Random;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;


/**
 * Created by mihha on 3/18/2017.
 */

public class Profile extends LoginActivity {
    private TextView nameView;
    private TextView emailView;
    private StorageReference mStorageRef;
    private int PICK_IMAGE_REQUEST = 1;
    BottomNavigationView bottomNavigationView;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiler);


        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lastOpen", "");
        editor.apply();

        LoginActivity.page = "Profile";
        mStorageRef = FirebaseStorage.getInstance().getReference();
        setNameAndEmail();
        getPicture();
        menu();


    }
    void setNameAndEmail(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        String name = user.getDisplayName();
        if (name == null){
            name="No name";
        }
        String email = user.getEmail();
        nameView = (TextView) findViewById(R.id.user_profile_name);
        emailView = (TextView) findViewById(R.id.user_profile_email);
        nameView.setText(name);
        emailView.setText(email);
    }
    void changeName(View view){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("CHANGE NAME");
        alert.setMessage("Enter your name");
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                addNewNameToBase(input.getText().toString());
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();

    }
    void addNewNameToBase(String name){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
//                            Toast.makeText(Profile.this, "Profile edited, resign your acc",
//                                    Toast.LENGTH_SHORT).show();


                        }
                    }
                });
        nameView = (TextView) findViewById(R.id.user_profile_name);
        nameView.setText(name);
        reload();
    }
    void reload(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Reload Page");
        alert.setMessage("Enter your password");
        final EditText inputPass = new EditText(this);
        inputPass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        inputPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
        alert.setView(inputPass);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                AuthCredential credential = EmailAuthProvider
                        .getCredential(emailView.getText().toString(), inputPass.getText().toString());

                user.reauthenticate(credential)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Log.d("", "User re-authenticated.");
                            }
                        });
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();

    }
    void logout(View view){
        FirebaseAuth.getInstance().signOut();
        Intent intent  = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    void getPicture(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user.getPhotoUrl() != null){
            final String imgURL = user.getPhotoUrl().toString();

            final ImageButton photo = (ImageButton) findViewById(R.id.user_profile_photo);
            new DownLoadImageTask(photo).execute(imgURL);
        }

    }
    private class DownLoadImageTask extends AsyncTask<String,Void,Bitmap> {
        ImageButton imageButton;

        public DownLoadImageTask(ImageButton imageButton){
            this.imageButton = imageButton;
        }

        protected Bitmap doInBackground(String...urls){
            String urlOfImage = urls[0];
            Bitmap logo = null;
            try{
                InputStream is = new URL(urlOfImage).openStream();
                logo = BitmapFactory.decodeStream(is);
            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        protected void onPostExecute(Bitmap result){
            Drawable d = new BitmapDrawable(getResources(), result);
            imageButton.setBackground(d);
        }
    }

    void changePicture(View view){
        AlertDialog alertDialog = new AlertDialog.Builder(Profile.this).create(); //Read Update
        alertDialog.setTitle("Change picture");

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                        PICK_IMAGE_REQUEST);

            }});
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Camera", new DialogInterface.OnClickListener()    {
            public void onClick(DialogInterface dialog, int which) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, PICK_IMAGE_REQUEST);

            }});
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"Cancel", new DialogInterface.OnClickListener()    {
            public void onClick(DialogInterface dialog, int which) {

            }});

        alertDialog.show();





    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                ImageView imageView = (ImageView) findViewById(R.id.user_profile_photo);
                Drawable d = new BitmapDrawable(getResources(), bitmap);
                imageView.setBackground(d);
                uploadPicture(imageView);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    void uploadPicture(ImageView imageView){
        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        StorageReference storageRef;
        storageRef = FirebaseStorage.getInstance().getReference();
        Random rn = new Random();
        int answer = rn.nextInt(100000000) + 1;
        StorageReference mountainsRef = storageRef.child(answer+".jpg");
        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests")
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                        .setPhotoUri(Uri.parse(downloadUrl.toString()))
                        .build();
                user.updateProfile(profileUpdates)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(Profile.this, "Profile edited, resign your acc",
                                Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

            }
        });
        reload();
    }

    void menu(){
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.profile) {
                    Intent intent  = new Intent(Profile.this, Profile.class);
                    startActivity(intent);
                    finish();
                    return true;
                }if (item.getItemId() == R.id.topic) {
                    Intent intent  = new Intent(Profile.this, Topic.class);
                    startActivity(intent);
                    finish();
                    return true;
                }if (item.getItemId() == R.id.createTopic) {
                    Intent intent  = new Intent(Profile.this, CreateTopic.class);
                    startActivity(intent);
                    finish();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }


}