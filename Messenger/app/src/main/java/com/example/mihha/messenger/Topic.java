package com.example.mihha.messenger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mihha on 3/20/2017.
 */

public class Topic extends Profile {
    private FirebaseListAdapter<TopicObject> adapter;
    private boolean isTrue = false;
    public static ArrayList<String> users=new ArrayList<>();
    public static Query query = FirebaseDatabase.getInstance().getReference("topics").orderByChild("numberOfUsers");



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);
        menu();
        query = FirebaseDatabase.getInstance().getReference("topics").orderByChild("numberOfUsers");
        new DisplayTopics().execute();
        checkForDelete();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

    }
    void checkForDelete(){

        FirebaseDatabase.getInstance().getReference("topics").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot issue: dataSnapshot.getChildren()) {
                            long messageTime = Long.parseLong(issue.child("lastMessageTime").getValue().toString());
                            if (messageTime!=0) {


                                long timeNow = new Date().getTime();
                                Date d1 = new Date(timeNow);
                                Date d2 = new Date(messageTime);
                                long diff = d1.getTime() - d2.getTime();


                                long diffMinutes = diff / (60 * 1000) % 60;
                                long diffHours = diff / (60 * 60 * 1000) % 24;
                                long diffDays = diff / (24 * 60 * 60 * 1000);
                                String type = issue.child("type").getValue().toString();
                                if (type.equals("normal") && (diffDays > 0 || diffHours > 0 || diffMinutes > 0)) {
                                    issue.getRef().setValue(null);

                                }
                            }

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }


    private class DisplayTopics extends AsyncTask<String,Void,Bitmap> {
        ListView listOfTopics;

        @Override
        protected Bitmap doInBackground(String... params) {
            listOfTopics = (ListView)findViewById(R.id.list_of_topics);

            adapter = new FirebaseListAdapter<TopicObject>(Topic.this, TopicObject.class,
                    R.layout.topic, query) {
                @Override
                protected void populateView(View v, TopicObject model, int position) {

                    // Get references to the views of message.xml
                    TextView topicText = (TextView)v.findViewById(R.id.topic);
                    TextView topicUsers = (TextView)v.findViewById(R.id.viewers);

                    TextView timeFromLastMessage = (TextView)v.findViewById(R.id.date);
                    long timeNow = new Date().getTime();
                    long messageTime = model.getLastMessageTime();



                    if (messageTime!=(0)){

                        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");


                        Date d1 = new Date(timeNow);
                        Date d2 = new Date(messageTime);

                        long diff = d1.getTime() - d2.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        long diffDays = diff / (24 * 60 * 60 * 1000);

                        timeFromLastMessage.setText("Last message " + diffHours+" h, " + diffMinutes + " min, " + diffSeconds + " sec.");
                    } else {
                        timeFromLastMessage.setText("empty");
                    }


                    int numberOfUsers = model.getNumberOfUsers();
                    String type = model.getType();
                    final ImageView picture = (ImageView) v.findViewById(R.id.picture);
                    String topic = model.getTopic();
                    topicText.setText(topic);
                    if (type!=null){
                        if (type.equals("normal")){
                            topicUsers.setText(numberOfUsers*(-1)+" users");
                        } else {
                            topicUsers.setText((numberOfUsers*(-1)-1000)+" users");
                        }
                    }



                    String pictureUrl = model.getPicture();
                    new DownLoadImageTask(picture).execute(pictureUrl);

                    // Format the date before showing it
//                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
//                        model.getMessageTime()));
                }


            };

            return null;
        }
        private class DownLoadImageTask extends AsyncTask<String,Void,Bitmap> {
            ImageView imageView;

            public DownLoadImageTask(ImageView imageView){
                this.imageView = imageView;
            }

            protected Bitmap doInBackground(String...urls){
                String urlOfImage = urls[0];
                Bitmap logo = null;
                try{
                    InputStream is = new URL(urlOfImage).openStream();
                    logo = BitmapFactory.decodeStream(is);
                }catch(Exception e){ // Catch the download exception
                    e.printStackTrace();
                }
                return logo;
            }

            protected void onPostExecute(Bitmap result){
                imageView.setImageBitmap(result);
            }
        }

        protected void onPostExecute(Bitmap res){
            if (listOfTopics != null){
                listOfTopics.setAdapter(adapter);
            }

        }
    }
    void openMessage(final View view){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("topics");
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot child: dataSnapshot.getChildren()){

                            TextView topicText = (TextView) view.findViewById(R.id.topic);
                            findTopic(child.getKey(), topicText.getText().toString());
                        }

                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });
    }
    void findTopic(final String topic, final String realTopic){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("topics").child(topic).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        TopicObject topicObject = dataSnapshot.getValue(TopicObject.class);
                        String topicString = topicObject.getTopic(); // "John Doe"
                        if (topicString.equals(realTopic)){
                            getChildren(topic);


                            Bundle extras = new Bundle();
                            extras.putString("topic",topic);
                            Intent intent  = new Intent(Topic.this, Chat.class);
                            intent.putExtras(extras);
                            startActivity(intent);
                            finish();
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    void getChildren(final String topic){
        if (users.size()!=0){
            users.clear();
        }
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("topics").child(topic).child("users");
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot child: dataSnapshot.getChildren()){
                            addUserToTopic(topic, child.getKey());
                        }



                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });

    }

    void addUserToTopic(final String topic, String child){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("topics/"+ topic +"/users/"+ child);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(User.class) != null) {
                    User user = dataSnapshot.getValue(User.class);
                    users.add(user.getName());


                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    void menu(){
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.topic);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.profile) {
                    Intent intent  = new Intent(Topic.this, Profile.class);
                    startActivity(intent);
                    finish();
                    return true;
                }if (item.getItemId() == R.id.topic) {
                    Intent intent  = new Intent(Topic.this, Topic.class);
                    startActivity(intent);
                    finish();
                    return true;
                }if (item.getItemId() == R.id.createTopic) {
                    Intent intent  = new Intent(Topic.this, CreateTopic.class);
                    startActivity(intent);
                    finish();
                    return true;
                }
                return false;
            }
        });
    }
    void searchTopic(View view){
        EditText editText = (EditText) findViewById(R.id.search);
        String searchedTopic = editText.getText().toString();
        if (!searchedTopic.equals("")){
            query = FirebaseDatabase.getInstance().getReference("topics").orderByChild("topic").startAt(searchedTopic).endAt(searchedTopic + "\uf8ff");
        }else {
            query = FirebaseDatabase.getInstance().getReference("topics").orderByChild("numberOfUsers");
        }
        new DisplayTopics().execute();


    }
    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }
}
