package com.example.mihha.messenger;



import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class LoginActivity extends AppCompatActivity {


    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    public static String page;
    public static SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mLoginFormView = findViewById(R.id.login_form);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d("", "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    boolean control(String email, String password){
        if (email!=null && password.length()>=6){
            return true;
        }
        Toast.makeText(LoginActivity.this, "Error",
                Toast.LENGTH_SHORT).show();
        return false;
    }
    void register(View view){
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        if (control(email, password)){
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d("", "createUserWithEmail:onComplete:" + task.isSuccessful());

                            if (!task.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, "Registration failed",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                enter();
                            }
                        }
                    });
        }
    }

    void signIn(View view){
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
//        String email = "mihha@gmail.com";
//        String password = "111111";
        if (control(email, password)) {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d("", "signInWithEmail:onComplete:" + task.isSuccessful());

                            // If sign in fails, display a message to the userName. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in userName can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Log.w("", "signInWithEmail:failed", task.getException());
                                Toast.makeText(LoginActivity.this, "Authentication failed",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                enter();
                            }
                        }
                    });


        }

    }
    void enter(){
        Intent intent  = new Intent(this, Profile.class);
        startActivity(intent);
        finish();
    }
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        preferences = getPreferences(MODE_PRIVATE);
//
//        String lastChoice = preferences.getString("lastOpen","");
//
//        Toast.makeText(LoginActivity.this, lastChoice,
//                Toast.LENGTH_SHORT).show();
//
////        if (lastChoice.equals("Profile")) {
////
////            Intent intent  = new Intent(LoginActivity.this, Profile.class);
////            startActivity(intent);
////            finish();
////
////        }if (lastChoice.equals("Topic")) {
////
////            Intent intent  = new Intent(LoginActivity.this, Topic.class);
////            startActivity(intent);
////            finish();
////
////        }if (lastChoice.equals("Chat")) {
////
////            Intent intent  = new Intent(LoginActivity.this, CreateTopic.class);
////            startActivity(intent);
////            finish();
////
////        }
//
//    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }




}

