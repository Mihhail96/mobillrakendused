package com.example.mihha.messenger;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by mihha on 3/21/2017.
 */

public class CreateTopic extends Topic {
    private static  final int PICK_IMAGE = 2;
    public static ArrayList<String> topicNames = new ArrayList<>();
    public static String picture = "https://firebasestorage.googleapis.com/v0/b/mihhos-78b61.appspot.com/o/nopicture.png?alt=media&token=d1b4e9f8-cb78-4417-9df8-2b267279ca14";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actiivity_create_topic);
        menu();
    }
    void createTopic(View view){



        watchTopics(view);


    }
    void watchTopics(final View view){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("topics");
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                topicNames.add(issue.child("topic").getValue().toString());
                            }
                        }
                        if (topicNames!=null){
                            EditText input = (EditText)findViewById(R.id.topicname);
                            String in = input.getText().toString();

                            for (String word : Dispatcher.curseWords) {
                                word = word.replaceAll("[^A-Za-z0-9]", "");
                                Pattern rx = Pattern.compile("\\b" + word + "\\b", Pattern.CASE_INSENSITIVE);
                                in = rx.matcher(in).replaceAll(new String(new char[word.length()]).replace('\0', '*'));
                            }

                            if (!topicNames.contains(in)){
                                FirebaseDatabase.getInstance()
                                        .getReference("topics")
                                        .push()
                                        .setValue(new TopicObject(in,
                                                picture, 0, 0,"normal"));

                                Intent intent  = new Intent(CreateTopic.this, Topic.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(CreateTopic.this, "Such topic already exists",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }



                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });
    }


    void menu(){
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.createTopic);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.profile) {
                    Intent intent  = new Intent(CreateTopic.this, Profile.class);
                    startActivity(intent);
                    finish();
                    return true;
                }if (item.getItemId() == R.id.topic) {
                    Intent intent  = new Intent(CreateTopic.this, Topic.class);
                    startActivity(intent);
                    finish();
                    return true;
                }if (item.getItemId() == R.id.createTopic) {
                    Intent intent  = new Intent(CreateTopic.this, CreateTopic.class);
                    startActivity(intent);
                    finish();
                    return true;
                }
                return false;
            }
        });
    }
    void changePicture(View view){
        AlertDialog alertDialog = new AlertDialog.Builder(CreateTopic.this).create(); //Read Update
        alertDialog.setTitle("Change picture");

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                        PICK_IMAGE);

            }});
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Camera", new DialogInterface.OnClickListener()    {
            public void onClick(DialogInterface dialog, int which) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, PICK_IMAGE);

            }});
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"Cancel", new DialogInterface.OnClickListener()    {
            public void onClick(DialogInterface dialog, int which) {

            }});

        alertDialog.show();  //<-- See This!
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                ImageButton imageButton = (ImageButton) findViewById(R.id.topicpicture);
                Drawable d = new BitmapDrawable(getResources(), bitmap);
                imageButton.setBackground(d);
                uploadPicture(imageButton);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    void uploadPicture(ImageView imageView){
        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        StorageReference storageRef;
        storageRef = FirebaseStorage.getInstance().getReference();
        Random rn = new Random();
        int answer = rn.nextInt(100000000) + 1;
        StorageReference mountainsRef = storageRef.child(answer+".jpg");
        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests")
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                CreateTopic.picture = downloadUrl.toString();


            }
        });
    }
    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }
}
