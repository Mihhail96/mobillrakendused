package com.example.mihha.messenger;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mihha on 3/29/2017.
 */

public class Dispatcher extends Activity {
    public static String curseWords[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AssetManager assetManager = getAssets();
        InputStream inputStream;
        try {
            inputStream = assetManager.open("text.txt");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();

            String text = new String(buffer);
            curseWords = text.split("\n");

        } catch (IOException e) {
            e.printStackTrace();
        }


        Class<?> activityClass;

        try {
            SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
            activityClass = Class.forName(
                    prefs.getString("lastActivity", LoginActivity.class.getName()));
        } catch(ClassNotFoundException ex) {
            activityClass =  LoginActivity.class;
        }

        startActivity(new Intent(this, activityClass));
    }
}