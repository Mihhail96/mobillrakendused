package com.example.mihha.messenger;

import java.util.Date;

/**
 * Created by mihha on 3/20/2017.
 */

public class TopicObject {

    private String topic;
    private String picture;
    private long lastMessageTime;
    private int numberOfUsers;
    private String type;


    public TopicObject(String topic, String picture, int numberOfUsers,long lastMessageTime,String type) {
        this.topic = topic;
        this.picture = picture;
        this.numberOfUsers = numberOfUsers;
        this.lastMessageTime = lastMessageTime;
        this.type = type;

    }

    public TopicObject(){

    }

    public String getTopic() {
        return topic;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getNumberOfUsers() {
        return numberOfUsers;
    }

    public void setNumberOfUsers(int numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }

    public long getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(long lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }
}