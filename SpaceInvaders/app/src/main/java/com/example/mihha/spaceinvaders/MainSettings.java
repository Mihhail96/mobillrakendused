package com.example.mihha.spaceinvaders;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

/**
 * Created by mihha on 5/7/2017.
 */

public class MainSettings extends Activity{
    private SeekBar seekBarEnemies;
    private SeekBar seekBarSpeed;
    private SeekBar seekBarBullets;
    private SeekBar seekBarSize;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        seekBarEnemies = (SeekBar) findViewById(R.id.seekBarEnemies);
        seekBarSpeed = (SeekBar) findViewById(R.id.seekBarSpeed);
        seekBarBullets = (SeekBar) findViewById(R.id.seekBarBullets);
        seekBarSize = (SeekBar) findViewById(R.id.seekBarSize);

        seekBars(seekBarEnemies);
        seekBars(seekBarSpeed);
        seekBars(seekBarSize);
        seekBars(seekBarBullets);



        SharedPreferences sPref = getSharedPreferences("X", MODE_PRIVATE);
        seekBarEnemies.setProgress(Integer.parseInt(sPref.getString("settingsEnemies", "15")));
        seekBarSpeed.setProgress(Integer.parseInt(sPref.getString("settingsSpeed", "10")));
        seekBarSize.setProgress(Integer.parseInt(sPref.getString("settingsSize", "150")));
        seekBarBullets.setProgress(Integer.parseInt(sPref.getString("settingsBullets", "20")));

    }

    private void seekBars(SeekBar seekBar){
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainSettings.this, seekBar.getProgress()+"", Toast.LENGTH_SHORT).show();
                updateSettings();
            }
    });
    }

    private void updateSettings(){
        SharedPreferences sPref = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("settingsEnemies", seekBarEnemies.getProgress()+"");
        ed.putString("settingsSpeed", seekBarSpeed.getProgress()+"");
        ed.putString("settingsSize", seekBarSize.getProgress()+"");
        ed.putString("settingsBullets", seekBarBullets.getProgress()+"");
        ed.commit();
    }
    public void changeNickname(View view){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Enter your nickname");
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                SharedPreferences sPref = getSharedPreferences("X", MODE_PRIVATE);
                SharedPreferences.Editor ed = sPref.edit();
                ed.putString("lastNickname", input.getText().toString());
                ed.commit();
                Toast.makeText(getApplicationContext(),
                        "Nick changed", Toast.LENGTH_SHORT).show();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }

}
