package com.example.mihha.spaceinvaders;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * Created by mihha on 5/7/2017.
 */

public class MainScoreBoard extends Activity {
    private FirebaseListAdapter<ListViewItem> adapter;
    public static Query query = FirebaseDatabase.getInstance().getReference().child("items").orderByChild("score");
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);


        new DisplayScore().execute();

    }

    private class DisplayScore extends AsyncTask<String, Void, Bitmap> {

        ListView listOfScores;

        @Override
        protected Bitmap doInBackground(String... params) {
            listOfScores = (ListView) findViewById(R.id.scoreboard);

            adapter = new FirebaseListAdapter<ListViewItem>(MainScoreBoard.this, ListViewItem.class,
                    R.layout.list_view_item, query) {
                @Override
                protected void populateView(View v, ListViewItem model, int position) {
                    TextView textViewNickname = (TextView) v.findViewById(R.id.nickname);
                    TextView textViewScore = (TextView) v.findViewById(R.id.score);
                    long score = model.getScore()*(-1);
                    String nickname = model.getNickname();
                    textViewNickname.setText(nickname);
                    textViewScore.setText(score +"");

                }
            };

            return null;
        }
        protected void onPostExecute(Bitmap res){
            if (listOfScores != null){
                listOfScores.setAdapter(adapter);
            }

        }


    }

}
