package com.example.mihha.spaceinvaders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by mihha on 5/7/2017.
 */

public class MainMenu extends Activity{
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

    }
    public void startGame(View view){
        Intent intent  = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    public void openSettings(View view){
        Intent intent  = new Intent(this, MainSettings.class);
        startActivity(intent);

    }
    public void openScoreBoard(View view){
        Intent intent  = new Intent(this, MainScoreBoard.class);
        startActivity(intent);

    }
}
