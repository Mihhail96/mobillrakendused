package com.example.mihha.spaceinvaders;

/**
 * Created by mihha on 5/8/2017.
 */

public class ListViewItem {
    private String nickname;
    private long score;

    public ListViewItem(String nickname, long score) {
        this.nickname = nickname;
        this.score = score;
    }

    public ListViewItem(){}


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }
}
