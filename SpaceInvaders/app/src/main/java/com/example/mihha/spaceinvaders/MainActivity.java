package com.example.mihha.spaceinvaders;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;

import java.util.Random;


public class MainActivity extends Activity{

    private static int enemiesNumber = 15;
    private static int enemiesSize = 150;
    private static int playerSize = 150;
    private static int maxBullets = 10;
    private static int maxSpeed = 10;
    private Drawable drawableShip;
    private Drawable drawableEnemy;
    private Drawable drawableBackground;
    private Drawable drawableBullet;
    private float screenWidth;
    private float screenHeight;
    private float touchControl = -1;

    private boolean fireBool;
    private Random random;

    private Paint scorePaint;
    private Game.Player player;
    private Game.Enemy[] enemyObjects;
    private Game.Bullet[] bulletObjects;
    private int maxGameScore;
    private boolean death = false;
    private SoundPool sounds;
    private int piu;



    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        SharedPreferences sPref = getSharedPreferences("X", MODE_PRIVATE);
        enemiesNumber =Integer.parseInt(sPref.getString("settingsEnemies", "15"));
        maxSpeed = Integer.parseInt(sPref.getString("settingsSpeed", "10"));
        enemiesSize = Integer.parseInt(sPref.getString("settingsSize", "150"));
        playerSize = Integer.parseInt(sPref.getString("settingsSize", "150"));
        maxBullets = Integer.parseInt(sPref.getString("settingsBullets", "10"));


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(new Game(this));

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        drawableBullet = getDrawable(R.drawable.laser);
        drawableShip = getDrawable(R.drawable.playership);
        drawableEnemy = getDrawable(R.drawable.spaceship);

        maxGameScore = Integer.parseInt(sPref.getString("maxScore", "0"));
        sounds = new SoundPool(10, AudioManager.STREAM_MUSIC,0);
        piu = sounds.load(this, R.raw.piu, 1);


    }




    private class Game extends View{

        public Game(Context context){
            super(context);
            random = new Random();
            scorePaint = new Paint();
            scorePaint.setTextSize(120);
            scorePaint.setFakeBoldText(true);
            scorePaint.setColor(Color.RED);

            player = new Player();
//Create enemyObjects
            enemyObjects = new Enemy[enemiesNumber];
            for(int i = 0; i != enemyObjects.length; i++){
                enemyObjects[i] = new Enemy();
            }

//Create bulletObjects
            bulletObjects = new Bullet[maxBullets];
            for(int i = 0; i != bulletObjects.length; i++){
                bulletObjects[i] = new Bullet();
            }

            Timer timer = new Timer();
        }


        @Override
        protected void onDraw(Canvas canvas){
            super.onDraw(canvas);

            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = metrics.widthPixels;
            screenHeight = metrics.heightPixels;

            drawableBackground = getDrawable(R.drawable.space);;
            drawableBackground.setBounds(0, 0, (int) screenWidth, (int) screenHeight);
            drawableBackground.draw(canvas);

            player.run(canvas);

            for(Bullet bullet : bulletObjects){
                bullet.run(canvas);
            }

            for(Enemy enemy : enemyObjects){
                enemy.run(canvas);
            }
        }




        private class Timer extends java.util.TimerTask{
            {
                java.util.Timer timer = new java.util.Timer();
                timer.scheduleAtFixedRate(this, 0, 5);
            }

            @Override
            public void run(){
                Game.this.postInvalidate();
            }
        }


        private class Player extends Enemy{
            private int score;

            @Override
            public void run(Canvas canvas){
                if (score<0){
                    gameOn =false;
                    death=true;
                }
                if(!gameOn){
//Restart
                    SharedPreferences sPref = getSharedPreferences("X", MODE_PRIVATE);
                    SharedPreferences.Editor ed = sPref.edit();
                    ed.putString("score", score+"");
                    ed.commit();

                    if (score>maxGameScore){
                        maxGameScore=score;
                        ed.putString("maxScore", maxGameScore+"");
                        ed.commit();
                    }
                    locationX = (screenWidth - playerSize) * 0.5f;
                    locationY = screenHeight  - playerSize -120 +100f;
                    gameOn = true;
                    score = 10;


                    for(Enemy enemy : enemyObjects){
                        enemy.gameOn = false;
                    }
                    if (death){
                        Intent intent  = new Intent(MainActivity.this, RestartMenu.class);
                        startActivity(intent);
                        finish();
                    }

                }else{
//Collision
                    for(Enemy enemy : enemyObjects){
                        if(locationX - playerSize /2 < enemy.locationX + enemiesSize
                                && locationX - playerSize /2 + enemiesSize > enemy.locationX
                                && locationY < enemy.locationY + enemiesSize
                                && locationY + enemiesSize > enemy.locationY){
                            gameOn = false;
                            death = true;
                        }
                    }

//Movement handler
                    if(touchControl != -1){
                        if(touchControl > locationX){
                            locationX += screenWidth * 0.016f;
                            if(touchControl < locationX){
                                locationX = touchControl;
                            }
                        }else if(touchControl < locationX){
                            locationX -= screenWidth * 0.016f;
                            if(touchControl > locationX){
                                locationX = touchControl;
                            }
                        }else{
                            touchControl = -1;
                        }

                        if(locationX < playerSize /2){
                            locationX = playerSize /2;
                        }else if(locationX >= screenWidth - playerSize /2){
                            locationX = screenWidth - playerSize /2;
                        }
                    }
                    drawableShip.setBounds((int) locationX - playerSize /2, (int) locationY - playerSize /2, (int) locationX + playerSize /2 + playerSize /8, (int) locationY + playerSize /2);
                    drawableShip.draw(canvas);
                    canvas.drawText("" + score, 0, screenHeight, scorePaint);
                }
            }
        }


        private class Bullet extends Enemy{
            @Override
            public void run(Canvas canvas){
                if(!gameOn && fireBool){
                    sounds.play(piu, 1.0f, 1.0f, 0, 0, 1.5f);
                    locationX = player.getLocationX() - enemiesSize / 3 +20f;
                    locationY = player.getLocationY();
                    fireBool = false;
                    gameOn = true;
                }else if(gameOn){
//Movement & collision
                    locationY -= 10;
                    for(Enemy enemy : enemyObjects){
                        if(locationX - playerSize /2 < enemy.locationX + enemiesSize
                                && locationX - playerSize /2 + enemiesSize > enemy.locationX
                                && locationY < enemy.locationY + enemiesSize
                                && locationY + enemiesSize > enemy.locationY){
                            enemy.gameOn = false;
                            gameOn = false;
                            player.score++;
                        }
                    }

//Deactivate
                    if(locationY < 0){
                        gameOn = false;
                    }

                    drawableBullet.setBounds((int) locationX, (int) locationY - playerSize /2, (int) locationX +2*(playerSize)/4, (int) locationY);
                    drawableBullet.draw(canvas);

                }
            }
        }
        private class Enemy{
            protected boolean gameOn;
            protected float locationX;
            protected float locationY;
            protected float speed;
            public float getLocationX(){
                return locationX;
            }
            public float getLocationY(){
                return locationY;
            }
            public void run(Canvas canvas){
//Create
                if(!gameOn){
                    locationX = random.nextInt((int) (screenWidth - enemiesSize));
                    locationY = random.nextInt((int) screenHeight) - screenHeight;
                    speed = (random.nextInt(maxSpeed) + 3) * 0.001f * screenHeight;
                    gameOn = true;

                }else{
//out of the map
                    if(locationX <= 0 || locationX >= screenWidth || locationY >= screenHeight){
                        gameOn = false;
                        player.score = player.score-2;
                    }else{
                        locationY += speed;

                        drawableEnemy.setBounds((int) locationX, (int) locationY - enemiesSize /2, (int) locationX +(enemiesSize) + enemiesSize /8, (int) locationY + enemiesSize /2);
                        drawableEnemy.draw(canvas);

                    }
                }
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent){
        if (motionEvent.getPointerCount() > 1) {
            fireBool = true;

        }
        switch(motionEvent.getAction() & MotionEvent.ACTION_MASK){
            case MotionEvent.ACTION_DOWN:
                fireBool = true;
            case MotionEvent.ACTION_MOVE:
                touchControl = motionEvent.getX();
                break;
            case MotionEvent.ACTION_UP:
                touchControl = -1;
            default:
                break;
        }

        return true;
    }
}
