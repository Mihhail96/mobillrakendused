package com.example.mihha.spaceinvaders;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by mihha on 5/7/2017.
 */

public class RestartMenu extends Activity{
    private int score;
    private int maxScore;
    private String lastNickname;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restart);
        SharedPreferences sPref = getSharedPreferences("X", MODE_PRIVATE);
        score=Integer.parseInt(sPref.getString("score", "0"));
        maxScore=Integer.parseInt(sPref.getString("maxScore", "0"));
        lastNickname = sPref.getString("lastNickname", "");
        TextView textViewScore = (TextView) findViewById(R.id.textViewScore);
        TextView textViewMaxScore = (TextView) findViewById(R.id.textViewMaxScore);
        textViewScore.setText("Your score: " + score);
        textViewMaxScore.setText("Max score: " + maxScore);
    }
    public void restart(View view){
        Intent intent  = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    public void menu(View view){
        Intent intent  = new Intent(this, MainMenu.class);
        startActivity(intent);
        finish();
    }
    public void addScore(View view){
        if (lastNickname.equals("")){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Enter your nickname");
            final EditText input = new EditText(this);
            alert.setView(input);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    addNickname(input.getText().toString());
                    SharedPreferences sPref = getSharedPreferences("X", MODE_PRIVATE);
                    SharedPreferences.Editor ed = sPref.edit();
                    ed.putString("lastNickname", input.getText().toString());
                    ed.commit();
                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });

            alert.show();

        }else {
            addNickname(lastNickname);
        }
    }




        private void addNickname(String nickname){
            FirebaseDatabase.getInstance()
                    .getReference("items")
                    .push()
                    .setValue(new ListViewItem(nickname, maxScore*(-1))).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(getApplicationContext(),
                            "Score added", Toast.LENGTH_SHORT).show();
                }
            });



        }



}
